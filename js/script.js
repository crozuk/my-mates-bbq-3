//Windows  (snapping)
$(document).ready(function(){
    var $windows = $('.window');
    $windows.windows({
        snapping: true,
        snapSpeed: 500,
        snapInterval: 1100,
        onScroll: function(s){},
        onSnapComplete: function($el){},
        onWindowEnter: function($el){}
    });
});
//Anchor SmoothScrolling
$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();
	    var target = this.hash,
	    $target = $(target);
	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});
//FitText
$(document).ready(function() {
    jQuery(".title").fitText(0.7);
});
//Sticky
$(document).ready(function(){
    $("#sticker").sticky({topSpacing: 0 });
})
//Navigation active states
$(document).ready(function(){

	//Add active state on reload
	$(function() {
		var loc = window.location.href; // returns the full URL
		if(/#page1/.test(loc)) {
			$('nav a').removeClass('active');
        	$('nav #link1').addClass('active');
        }
	});
	$(function() {
		var loc = window.location.href; // returns the full URL
		if(/#page2/.test(loc)) {
			$('nav a').removeClass('active');
        	$('nav #link2').addClass('active');
        }
	});
	$(function() {
		var loc = window.location.href; // returns the full URL
		if(/#page3/.test(loc)) {
			$('nav a').removeClass('active');
        	$('nav #link3').addClass('active');
        }
	});
	$(function() {
		var loc = window.location.href; // returns the full URL
		if(/#page4/.test(loc)) {
			$('nav a').removeClass('active');
        	$('nav #link4').addClass('active');
        }
	});
	$(function() {
		var loc = window.location.href; // returns the full URL
		if(/#page5/.test(loc)) {
			$('nav a').removeClass('active');
        	$('nav #link5').addClass('active');
        }
	});
	$(function() {
		var loc = window.location.href; // returns the full URL
		if(/#page6/.test(loc)) {
			$('nav a').removeClass('active');
        	$('nav #link6').addClass('active');
        }
	});

	//Add active state on click
    $('nav a').click(function(){
         $('nav a').removeClass('active');
         $(this).addClass('active');
    });

});
//SuperScroll
$(document).ready(function() {



            var controller = $.superscrollorama();
            // individual element tween examples
            controller.addTween('#1', TweenMax.from( $('.p2'), 1.2, {css:{opacity: 0}}),0 , 200);
            controller.addTween('#1', TweenMax.from( $('.p2a'), 1.2, {css:{opacity: 0}}),0 , 200);

            controller.addTween('#2', TweenMax.from( $('.p3'), 1.2, {css:{opacity: 0}}),0 , 200);
            controller.addTween('#2', TweenMax.from( $('.p3a'), 1.2, {css:{opacity: 0}}),0 , 200);

            controller.addTween('#3', TweenMax.from( $('.p4'), 1.2, {css:{opacity: 0}}),0 , 200);
            controller.addTween('#3', TweenMax.from( $('.p4a'), 1.2, {css:{opacity: 0}}),0 , 200);

            controller.addTween('#4', TweenMax.from( $('.p5'), 1.2, {css:{opacity: 0}}),0 , 200);
            controller.addTween('#4', TweenMax.from( $('.p5a'), 1.2, {css:{opacity: 0}}),0 , 200);

            controller.addTween('#5', TweenMax.from( $('.p6'), 1.2, {css:{opacity: 0}}),0 , 200);
            controller.addTween('#5', TweenMax.from( $('.p6a'), 1.2, {css:{opacity: 0}},0 , 200));

            controller.addTween('#5', TweenMax.from( $('.frame'), 1.2, {css:{width: '500px'}}),0 , 200);


});


// Spotify Resizing
$(document).ready(function () {

    $('#link6').click(function (event) {
        $('iframe').fadeOut();
    });

    var controller = $.superscrollorama();
    controller.addTween('#5', TweenMax.to( $('iframe'), 0.5, {css:{height: '500px'}}),0 , 200);
    controller.addTween('#5', TweenMax.to( $('iframe'), 0.5, {css:{width: '500px'}, onComplete: function(){$('iframe').fadeIn('fast')}}),0 , 200);

});